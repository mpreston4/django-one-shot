from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.

def todo_list_list(request):             # The request is the first argument to the todo_list_list function
    todolists = TodoList.objects.all()   # This line gets all of the TodoList objects, and saves in todolists variable
    context = {                          # This line and the next, stores todolists into the context dictionary
        "todolists": todolists            # which allows us to call the key in this dictionary in the html file.
    }
    return render(request, "todos/list.html", context)  # Render combines a given template (html file) with a given context dictionary,
# and returns an http response object with tath rendered text.


def todo_list_detail(request, id):        # The request here is "todos/<int:id>/"
    detail = get_object_or_404(TodoList, id=id)  # This line saves the specific TodoList object into the detail variable.
    context = {                                 # These lines store the detail variable into the context dictionary
        "detail": detail                        # and we assign a key to the value.
    }
    return render(request, "todos/detail.html", context)  # This combines the template (todos/detail.html) & context


def todo_list_create(request):   # The request here is "todos/create/"
    if request.method == "POST":   # If the request.method == POST
        form = TodoListForm(request.POST)
        if form.is_valid():       # make sure form is valid (all fields are filled out)
            list = form.save()    # save form into the list variable. we will use this to redirect to instance id below
            return redirect("todo_list_detail",id=list.id) # redirect user to "todo_list_deail/id"
    else:
        form = TodoListForm()  # The http request (todos/create/) renders this form. The form
        # comes from the forms.py file.
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):    # The request here is "todos/<int:id>/edit/"
    to_do_list = get_object_or_404(TodoList, id=id)
    if request.method== "POST":
        form = TodoListForm(request.POST,instance=to_do_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail",id=id)  # for redirect function, we need to pass the url path as the argument
    else:
        form = TodoListForm(instance=to_do_list)

    context = {
        "to_do_list_object": to_do_list,
        "to_do_form": form,
    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):   # The request here is "todos/<int:id>/delete/"
    list_delete = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        list_delete.delete()
        return redirect("todos")

    return render(request, "todos/delete.html")  #refresher, what does render do?


def todo_item_create(request):   # The request here is "todos/items/create"
    if request.method == "POST":
        item_form = TodoItemForm(request.POST)
        if item_form.is_valid():
            item = item_form.save()
            return redirect ("todo_list_detail", id=item.list.id)
    else:
        item_form = TodoItemForm()
    context = {
        "item_form": item_form
    }
    return render (request, "todos/create-item.html", context)
